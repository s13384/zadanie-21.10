
public interface AlarmListener //extends Runnable
{

	public void alarmTurnedOn(EnteredPinEvent event);
	public void alarmTurnedOff(EnteredPinEvent event);
}
