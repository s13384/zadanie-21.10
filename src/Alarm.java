import java.util.ArrayList;
import java.util.Scanner;


public class Alarm {

	private ArrayList<AlarmListener> listeners;
	private String pin;
	private Thread thread = new Thread(){
		
		public void run()
		{
			while (true)
			{
				Scanner sc = new Scanner(System.in);
				System.out.println("Podaj pin");
				enterPin(sc.nextLine());
			}
		}
	};
	
	Alarm(String pin)
	{
		listeners=new ArrayList<AlarmListener>();
		this.pin=pin;
	}
	public void addListener(AlarmListener listener)
	{
		listeners.add(listener);
	}
	public void removeAlarmListener(AlarmListener listener)
	{
		listeners.remove(listener);
	}
	public void enterPin(String pin)
	{
		System.out.println("Pin Alarmu : " + this.pin);
		System.out.println("Podany Pin : " + pin);
		
		if (this.pin.equals((pin)))
		{
			correctEnteredPin();
		}
		else
		{
			wrongEnteredPin();
		}
	}
	private void correctEnteredPin()
	{
		System.out.println("Podano poprawny pin");
		EnteredPinEvent event = new EnteredPinEvent(this);
		for (AlarmListener listener : listeners)
		{
			listener.alarmTurnedOff(event);
		}
	}
	private void wrongEnteredPin()
	{
		System.out.println("Podano nie poprawny pin");
		EnteredPinEvent event = new EnteredPinEvent(this);
		for (AlarmListener listener : listeners)
		{
			listener.alarmTurnedOn(event);
		}
	}
	public void odpalaj()
	{
		thread.start();
	}
	
}
