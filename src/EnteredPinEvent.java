import java.util.Date;


public class EnteredPinEvent{

	private Alarm alarm;
	private Date eventDate;
	
	EnteredPinEvent(Alarm alarm)
	{
		this.alarm = alarm;
		eventDate = new Date();
	}
	public Alarm getAlarm()
	{
		return alarm;
	}
	public Date getEventDate()
	{
		return eventDate;
	}
}
